package
import

/**
 * Class description
 *
 * @author name (i.e. John Smith)
 *
 * @version version_number
 * @since YYYY-MM-DD
 * 
 */
 
public class Example {
	
	
  /**
   * Description of what this constant represents
   *
   */
   
   private static final int Y = 0;
   
   
   /**
    * Description of what each instance variable represents
    * 
    */
    
   private int x;
   
   
   /**
    * Constructor for the Example class.
    *
    */
    
	 public Example (){
	 }
	 
	 
	 /**
	  * Constructor for the Example class with
	  * an argument.
	  *
	  * @param obj -> Object instance representing...
	  *
	  */
	  
	 public Example (Object obj){
	 }
	 
	 
	 
	 /**
	  * Description of what the method does
	  * 
	  * @return variable_name -> integer representing...
	  * 
	  */
	 
	 public int f(){
	 }
	 
	 
	 /**
	  * Description of what the method does
	  * 
	  * @param h -> integer representing...
	  * @return variable_name -> integer representing...
	  * 
	  */
	 
	 public int f(int h){
	 }
	 
	 
	 /**
	  * Description of what the method does
	  * 
	  * @return variable_name -> String representing...
	  * 
	  */
	 
	 public String j(){
	 }
	 
	 
	 /**
	  * Description of what the method does
	  * 
	  * @param g -> String representing...
	  * @return variable_name -> String representing...
	  * 
	  */
	 
	 public String j(String g){
	 }
	 
}